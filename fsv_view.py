from numpy import *
from PyQt4 import Qt
import PyQt4.Qwt5 as Qwt
import fsv

class App(Qt.QWidget):
    def __init__(self, *args):
        Qt.QWidget.__init__(self, *args)
        self.layout=Qt.QGridLayout(self)
        self.plt=Qwt.QwtPlot()
        self.plt.setAxisAutoScale(True)
        
def make():
    demo = App()
    demo.show()
    return demo

def main(args):
    app = Qt.QApplication(args)
    demo = make()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main(sys.argv)
