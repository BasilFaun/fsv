from numpy import sum,absolute
from numpy.fft import fft,ifft

class FSV(object):
    def __init__(self,d1,d2):
        if d1.shape!=d2.shape or d1.shape[0]<5 or d2.shape[0]<5:
            raise ValueError
        self.len=d1.shape[0]
        fd1=fft(d1)
        fd2=fft(d2)
        sfd1=sum(absolute(fd1[4:]))
        sfd2=sum(absolute(fd2[4:]))
        i=5
        psfd1,pasfd2=0.0,0.0
        while psfd1<0.4*sfd1 and psfd2<0.4*sfd2:
            psfd1+=absolute(fd1[i])
            psfd2+=absolute(fd2[i])
            i+=1
        self.brp=i
        winL=ones(self.len)
        winL[0:4]=0.0
        winL[self.brp-3:self.brp+3]=linspace(1.0,0.0,6)
        winL[self.brp+3:]=0.0
        self.Lo1=ifft(fd1*winL)
        self.Lo2=ifft(fd2*winL)
        winH=ones(self.len)
        winH[0:self.brp-3]=0.0
        winH[self.brp-3:self.brp+3]=linspace(0.0,1.0,6)
        self.Hi1=ifft(fd1*winH)
        self.Hi2=ifft(fd2*winH)

    def calc_hist(self,data):
        hist=zeros(6)
        for e in data:
            ae=absolute(e)
            if ae<0.1:
                hist[0]+=1
            elif 0.1<=ae<0.2:
                hist[1]+=1
            elif 0.2<=ae<0.4:
                hist[2]+=1
            elif 0.4<=ae<0.8:
                hist[3]+=1
            elif 0.8<=ae<1.6:
                hist[4]+=1
            elif 1.6<=ae:
                hist[5]+=1

                
    ##form may be 'p' - is a point by point ADM
    ##'s' is a single value of ADM 
    ##'h' is a ADM confidence histogram
    def adm(self,form='p'):
        if not hasattr(self,'admi'):
            self.admi=absolute(absolute(self.Lo1)-absolute(self.Lo2))/(sum(absolute(self.Lo1)+absolute(self.Lo2))/self.len)
        if form=='p':
            return self.admi
        elif form=='s':
            if not hasattr(self,'adms'):
                self.adms=sum(self.admi)/self.len
            return self.adms
        elif form=='h':
            if not hasattr(self,'admh'):
                self.admh=self.calc_hist(self.admi)
            return self.admh

    def fdm(self,form='p'):
        if not hasattr(self,'fdmi'):
            dlo1=absolute(diff(self.Lo1))
            dlo2=absolute(diff(self.Lo2))
            dhi1=diff(self.Hi1)
            dhi2=diff(self.Hi2)
            ddhi1=absolute(diff(dhi1))
            ddhi2=absolute(diff(dhi2))
            dhi1=absolute(dhi1)
            dhi2=absolute(dhi2)
            fdm1=(dlo1-dlo2)/(2*sum(dlo1+dlo2)/self.len)
            fdm2=(dhi1-dhi2)/(6*sum(dhi1+dhi2)/self.len)
            fdm3=(ddhi1-ddhi2)/(7.2*sum(ddhi1+ddhi2)/self.len)
            self.fdmi=2*absolute(fdm1[:-2]+fdm2[:-1]+fdm3)
        if form=='p':
            return self.fdmi
        elif form=='s':
            if not hasattr(self,'fdms'):
                self.fdms=sum(self.fdmi)/self.len
            return self.fdms
        elif form=='h':
            if !hasattr(self,'fdmh'):
                self.fdmh=self.calc_hist(self.fdmi)
            return self.fdmh
    def gdm(self):
        pass
